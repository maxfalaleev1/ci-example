FROM php:7.0
WORKDIR /app
COPY . .
CMD ["./run.sh"]